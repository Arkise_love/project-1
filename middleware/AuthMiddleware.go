package middleware

import (
	"project-1/utils"

	"strings"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)


func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			err := UnauthorizedError.New("Unauthorized: No token provided")
			c.Set("error", errorx.EnsureStackTrace(err))
			c.AbortWithStatusJSON(401, gin.H{"error": "Unauthorized: No token provided"})
			return
		}

		tokenString := strings.TrimSpace(strings.Replace(authHeader, "Bearer", "", 1))

		user, err := utils.VerifyToken(tokenString)
		if err != nil {
			err := UnauthorizedError.Wrap(err, "Unauthorized: Invalid token")
			c.Set("error", errorx.EnsureStackTrace(err))
			c.AbortWithStatusJSON(401, gin.H{"error": "Unauthorized: Invalid token"})
			return
		}

		c.Set("user", user)
		c.Set("userID", user.ID)
		c.Next()
	}
}
