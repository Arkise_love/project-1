package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)
var (
	Namespace = errorx.NewNamespace("project1_service")

	UnableToSaveError         = Namespace.NewType("unable_to_save")
	UnableToFindResourceError = Namespace.NewType("unable_to_find_resource")
	UnableToReadError         = Namespace.NewType("unable_to_read")
	UnauthorizedError         = Namespace.NewType("unauthorized")
	DataBaseError             = Namespace.NewType("data_base_error")
)

func ErrorHandlerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		if err, exists := c.Get("error"); exists {
			if e, ok := err.(error); ok {
				switch {
				case errorx.IsOfType(e, UnableToSaveError):
					c.JSON(http.StatusInternalServerError, gin.H{"error": e.Error()})
				case errorx.IsOfType(e, UnableToFindResourceError):
					c.JSON(http.StatusNotFound, gin.H{"error": e.Error()})
				case errorx.IsOfType(e, UnableToReadError):
					c.JSON(http.StatusInternalServerError, gin.H{"error": e.Error()})
				case errorx.IsOfType(e, UnauthorizedError):
					c.JSON(http.StatusUnauthorized, gin.H{"error": e.Error()})
				case errorx.IsOfType(e, DataBaseError):
					c.JSON(http.StatusInternalServerError, gin.H{"error": e.Error()})
				default:
					c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal Server Error"})
				}
			} else {
				c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal Server Error"})
			}
		}
	}
}
