package models

type PhoneNumber string

type User struct {
	ID          int         `json:"id,omitempty"`
	Username    string      `json:"username,omitempty"`
	Password    string      `json:"password,omitempty"`
	Email       string      `json:"email,omitempty"`
	PhoneNumber PhoneNumber `json:"phone_number,omitempty"`
	Address     string      `json:"address,omitempty"`
}

