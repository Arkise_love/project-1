package config

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	DBConnection string
	DBHost       string
	DBPort       string
	DBName       string
	DBUsername   string
	DBPassword   string
}

func LoadConfig(logger log.Logger) (*Config, error) {
	if err := godotenv.Load(); err != nil {
		return nil, err
	}
	config := &Config{
		DBConnection: os.Getenv("DB_CONNECTION"),
		DBHost:       os.Getenv("DB_HOST"),
		DBPort:       os.Getenv("DB_PORT"),
		DBName:       os.Getenv("DB_DATABASE"),
		DBUsername:   os.Getenv("DB_USERNAME"),
		DBPassword:   os.Getenv("DB_PASSWORD"),
	}

	return config, nil
}

func (c *Config) DSN() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", 
		c.DBHost, c.DBPort, c.DBUsername, c.DBPassword, c.DBName)
}
