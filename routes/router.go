package routes

import (
	"database/sql"
	"project-1/data/imageRepository"
	"project-1/data/userRepository"
	"project-1/handlers"
	"project-1/middleware"
	"project-1/service/image"
	"project-1/service/user"

	"github.com/gin-gonic/gin"
)

func SetupRouter(db *sql.DB) *gin.Engine {
	userRepo := &userRepository.UserRepository{DB: db}
	imageRepo := &imageRepository.ImageRepository{DB: db}

	userService := &user.UserServiceImpl{Repo: userRepo}
	imageService := &image.ImageServiceImpl{Repo: imageRepo}

	router := gin.Default()

	// Apply AuthMiddleware to routes that require authentication
	protected := router.Group("/")
	protected.Use(middleware.AuthMiddleware())
	{
		protected.POST("/upload-image", func(c *gin.Context) {
			handlers.UploadImageHandler(c, imageService)
		})
		protected.GET("/users", func(c *gin.Context) {
			handlers.ListUsersHandler(c, userService)
		})
		protected.POST("/refresh-token", func(c *gin.Context) {
			handlers.RefreshTokenHandler(c, userService)
		})
	}

	router.POST("/register", func(c *gin.Context) {
		handlers.RegisterHandler(c, userService)
	})
	router.POST("/login", func(c *gin.Context) {
		handlers.LoginHandler(c, userService)
	})

	return router
}
