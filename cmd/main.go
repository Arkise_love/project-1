package main

import (
	"database/sql"
	"log"
	"project-1/routes"

	_ "github.com/lib/pq" // PostgreSQL driver
)

func main() {

	db, err := sql.Open("postgres", "your_connection_string")
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	router := routes.SetupRouter(db)

	if err := router.Run(":8080"); err != nil {
		log.Fatalf("Failed to run server: %v", err)
	}
}
