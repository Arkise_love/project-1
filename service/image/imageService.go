package image

import (
	"context"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"project-1/data/imageRepository"
	"project-1/models"
	"project-1/utils"
)

const (
	imageUploadPath = "./images/"
	maxUploadSize   = 32 << 20
)

type ImageServiceImpl struct {
	Repo imageRepository.ImageRepositoryInterface
}

func (s *ImageServiceImpl) UploadImage(ctx context.Context, file *multipart.FileHeader, userID int64) (string, error) {
	var fileExt string
	switch file.Header.Get("Content-Type") {
	case "image/jpeg":
		fileExt = ".jpg"
	case "image/png":
		fileExt = ".png"
	case "image/gif":
		fileExt = ".gif"
	case "image/bmp":
		fileExt = ".bmp"
	default:
		return "", utils.ErrUnsupportedImageType
	}

	err := os.MkdirAll(imageUploadPath, os.ModePerm)
	if err != nil {
		return "", err
	}

	filename := filepath.Join(imageUploadPath, utils.GenerateRequestID()+fileExt)

	newFile, err := os.Create(filename)
	if err != nil {
		return "", err
	}
	defer newFile.Close()

	src, err := file.Open()
	if err != nil {
		return "", err
	}
	defer src.Close()

	_, err = io.Copy(newFile, src)
	if err != nil {
		return "", err
	}

	image := models.Image{
		UserID:   int(userID),
		Path: filename,
	}

	if err := s.Repo.SaveImage(ctx, &image); err != nil {
		return "", err
	}

	return filename, nil
}
