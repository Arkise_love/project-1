package image

import (
	"context"
	"mime/multipart"
)

type ImageService interface {
	UploadImage(ctx context.Context, file *multipart.FileHeader, userID int64) (string, error)
}
