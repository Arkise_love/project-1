package user

import (
	"context"
	"database/sql"
	"project-1/data/userRepository"
	"project-1/models"
	"project-1/utils"

	"golang.org/x/crypto/bcrypt"
)

type UserServiceImpl struct {
	Repo userRepository.UserRepositoryInterface
}

func (s *UserServiceImpl) Register(ctx context.Context, userInput models.User) error {
	if err := userInput.Validate(); err != nil {
		return err
	}

	user, err := s.Repo.FindUserByUsername(ctx, userInput.Username)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	if user != nil {
		return utils.ErrUserExists
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userInput.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	userData := models.User{
		Username:    userInput.Username,
		Password:    string(hashedPassword),
		Email:       userInput.Email,
		PhoneNumber: userInput.PhoneNumber,
		Address:     userInput.Address,
	}

	if err := s.Repo.CreateUser(ctx, &userData); err != nil {
		return err
	}

	return nil
}

func (s *UserServiceImpl) Login(ctx context.Context, user models.User) (string, error) {
	storedUser, err := s.Repo.FindUserByUsername(ctx, user.Username)
	if err != nil {
		return "", err
	}

	if storedUser == nil || !utils.ComparePasswords(storedUser.Password, user.Password) {
		return "", utils.ErrInvalidCredentials
	}

	tokenString, err := utils.GenerateToken(*storedUser)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (s *UserServiceImpl) ListUsers(ctx context.Context, page, limit int) ([]models.User, int, error) {
	offset := (page - 1) * limit
	users, err := s.Repo.ListUsers(ctx, offset, limit)
	if err != nil {
		return nil, 0, err
	}

	totalUsers, err := s.Repo.GetTotalUsersCount(ctx)
	if err != nil {
		return nil, 0, err
	}

	return users, totalUsers, nil
}

func (s *UserServiceImpl) RefreshToken(ctx context.Context, user models.User) (string, error) {
	storedUser, err := s.Repo.FindUserByUsername(ctx, user.Username)
	if err != nil {
		return "", err
	}

	if storedUser == nil {
		return "", utils.ErrUserNotFound
	}

	if !utils.ComparePasswords(storedUser.Password, user.Password) {
		return "", utils.ErrInvalidCredentials
	}

	tokenString, err := utils.GenerateToken(*storedUser)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
