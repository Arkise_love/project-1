package user

import (
	"context"
	"project-1/models"
)

type UserService interface {
	Register(ctx context.Context, user models.User) error
	Login(ctx context.Context, user models.User) (string, error)
	ListUsers(ctx context.Context, page, limit int) ([]models.User, int, error)
	RefreshToken(ctx context.Context, user models.User) (string, error)
}
