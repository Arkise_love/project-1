package handlers

import (
	"mime/multipart"
	"net/http"
	"project-1/service/image"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)

const (
	imageUploadPath = "./images/"
	maxUploadSize   = 32 << 20
)

type ImageUpload struct {
	Image *multipart.FileHeader `form:"images" binding:"required"`
}

func UploadImageHandler(c *gin.Context, imageService image.ImageService) {
	userID, exists := c.Get("userID")
	if !exists {
		err := errorx.IllegalState.New("Unauthorized")
		c.Set("error", err)
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	var imageUpload ImageUpload
	if err := c.ShouldBind(&imageUpload); err != nil {
		err = errorx.IllegalState.Wrap(err, "Invalid image upload parameters")
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	filename, err := imageService.UploadImage(c.Request.Context(), imageUpload.Image, userID.(int64))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"filename": filename})
}
