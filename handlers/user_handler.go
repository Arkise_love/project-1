package handlers

import (
	"net/http"
	"project-1/models"
	"project-1/service/user"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)
type MetaData struct {
	Page    int `json:"page"`
	PerPage int `json:"per_page"`
	Total   int `json:"total"`
}

type Pagination struct {
	Page  int `form:"page" binding:"omitempty,min=1"`
	Limit int `form:"limit" binding:"omitempty,min=1"`
}

func RegisterHandler(c *gin.Context, userService user.UserService) {
	var userInput models.User
	if err := c.BindJSON(&userInput); err != nil {
		err = errorx.IllegalState.Wrap(err, "Unable to read user input")
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := userService.Register(c.Request.Context(), userInput); err != nil {
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.String(http.StatusOK, "User registered successfully!")
	c.IndentedJSON(http.StatusOK, gin.H{"user": userInput})
}

func LoginHandler(c *gin.Context, userService user.UserService) {
	var user models.User
	if err := c.BindJSON(&user); err != nil {
		err = errorx.IllegalState.Wrap(err, "Failed to bind JSON")
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tokenString, err := userService.Login(c.Request.Context(), user)
	if err != nil {
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"Message": "User successfully logged in!",
		"Token":   tokenString,
		"User":    user.Username,
	})
}

func ListUsersHandler(c *gin.Context, userService user.UserService) {
	var pagination Pagination

	if err := c.ShouldBindQuery(&pagination); err != nil {
		err = errorx.IllegalState.Wrap(err, "Invalid pagination parameters")
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if pagination.Page == 0 {
		pagination.Page = 1
	}
	if pagination.Limit == 0 {
		pagination.Limit = 10
	}
	ctx := c.Request.Context()

	users, totalUsers, err := userService.ListUsers(ctx, pagination.Page, pagination.Limit)
	if err != nil {
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	metaData := MetaData{
		Page:    pagination.Page,
		PerPage: pagination.Limit,
		Total:   totalUsers,
	}

	successResponse := struct {
		MetaData MetaData      `json:"meta_data"`
		Data     []models.User `json:"data"`
	}{
		MetaData: metaData,
		Data:     users,
	}

	c.JSON(http.StatusOK, successResponse)
}

func RefreshTokenHandler(c *gin.Context, userService user.UserService) {
	var user models.User

	if err := c.BindJSON(&user); err != nil {
		err = errorx.IllegalState.Wrap(err, "Unable to read request")
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tokenString, err := userService.RefreshToken(c.Request.Context(), user)
	if err != nil {
		c.Set("error", errorx.EnsureStackTrace(err))
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Token refreshed successfully!",
		"token":   tokenString,
		"User":    user.Username,
	})
}
