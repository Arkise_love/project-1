package utils

import (
	"fmt"
	"project-1/models"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

var jwtKey = []byte("secret_key")

const TokenExpiration = 1 * time.Hour

func GenerateToken(user models.User) (string, error) {
    expirationTime := time.Now().Add(TokenExpiration)

    claims := jwt.MapClaims{
        "id": user.ID,
        "exp": expirationTime.Unix(), 
    }

    token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

    tokenString, err := token.SignedString(jwtKey)
    if err != nil {
        return "", err
    }
    return tokenString, nil
}

func VerifyToken(tokenString string) (*models.User, error) {
    token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
        }
        return jwtKey, nil
    })
    if err != nil {
        return nil, err
    }

    if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
        userID := int(claims["id"].(float64))

        user := &models.User{
            ID: userID,
        }
        return user, nil
    } else {
        return nil, fmt.Errorf("invalid token")
    }
}

func GenerateRequestID() string {
    return uuid.New().String()
}

func ComparePasswords(hashedPassword string, plaintextPassword string) bool {
    err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plaintextPassword))
    return err == nil
}
var (
	ErrUnsupportedImageType   = fmt.Errorf("unsupported image type")
	ErrUserExists             = fmt.Errorf("user already exists")
	ErrInvalidCredentials     = fmt.Errorf("invalid credentials")
	ErrUserNotFound           = fmt.Errorf("user not found")
)