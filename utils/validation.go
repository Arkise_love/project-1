package utils

import (
	"encoding/json"
	"project-1/models"
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type PhoneNumber string

func (p *PhoneNumber) UnmarshalJSON(data []byte) error {
	var number string
	if err := json.Unmarshal(data, &number); err != nil {
		return err
	}

	re := regexp.MustCompile(`^(\+2519|09|9|2519)\d{8}$`)
	if !re.MatchString(number) {
		return nil
	}

	last8Digits := number[len(number)-8:]
	newPhoneNumber := "2519" + last8Digits

	*p = PhoneNumber(newPhoneNumber)
	return nil
}

func (u models.User) Validate() error {
	err := validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required, validation.Length(0, 50)),
		validation.Field(&u.Password, validation.Required, validation.Length(6, 100)),
		validation.Field(&u.Email, is.Email),
		validation.Field(&u.PhoneNumber, validation.By(validatePhoneNumber)),
		validation.Field(&u.Address, validation.By(validateAddress)),
	)

	if err != nil {
		return err
	}

	return nil
}

func validatePhoneNumber(value interface{}) error {
	phone, _ := value.(PhoneNumber)
	re := regexp.MustCompile(`^2519\d{8}$`)
	if !re.MatchString(string(phone)) {
		return err
	}
	return nil
}

func validateAddress(value interface{}) error {
	address, _ := value.(string)
	re := regexp.MustCompile(`\d`)
	if re.MatchString(address) {
		return err
	}
	return nil
}
