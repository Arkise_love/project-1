package imageRepository

import (
	"context"
	"database/sql"
	"project-1/models"
)

type ImageRepository struct {
	DB *sql.DB
}

func (r *ImageRepository) SaveImage(ctx context.Context, image *models.Image) error {
	query := "INSERT INTO images (user_id, image_url) VALUES ($1, $2)"
	_, err := r.DB.ExecContext(ctx, query, image.UserID, image.Path)
	if err != nil {
		return err
	}
	return nil
}

func (r *ImageRepository) FindImageByID(ctx context.Context, id int64) (*models.Image, error) {
	query := "SELECT id, user_id, image_url FROM images WHERE id = $1"
	row := r.DB.QueryRowContext(ctx, query, id)
	image := &models.Image{}
	if err := row.Scan(&image.ID, &image.UserID, &image.Path); err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		return nil, err
	}
	return image, nil
}

func (r *ImageRepository) AddImage(ctx context.Context, userID int64, imageURL string) error {
	query := "INSERT INTO images (user_id, image_url) VALUES ($1, $2)"
	_, err := r.DB.ExecContext(ctx, query, userID, imageURL)
	if err != nil {
		return err
	}
	return nil
}

func (r *ImageRepository) OpenImage(ctx context.Context, id int64) (*models.Image, error) {
	return r.FindImageByID(ctx, id)
}

func (r *ImageRepository) DeleteImage(ctx context.Context, id int64) error {
	query := "DELETE FROM images WHERE id = $1"
	_, err := r.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}
	return nil
}
