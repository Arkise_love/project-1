package imageRepository

import (
	"context"
	"project-1/models"
)

type ImageRepositoryInterface interface {
	AddImage(ctx context.Context, userID int64, imageURL string) error
	SaveImage(ctx context.Context, image *models.Image) error
	FindImageByID(ctx context.Context, id int64) (*models.Image, error)
	OpenImage(ctx context.Context, id int64) (*models.Image, error)
	DeleteImage(ctx context.Context, id int64) error
}
