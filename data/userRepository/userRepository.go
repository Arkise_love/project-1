package userRepository

import (
	"context"
	"database/sql"
	"project-1/models"
)

type UserRepository struct {
	DB *sql.DB
}

func (r *UserRepository) CreateUser(ctx context.Context, user *models.User) error {
	query := "INSERT INTO users (username, email, password) VALUES ($1, $2, $3)"
	_, err := r.DB.ExecContext(ctx, query, user.Username, user.Email, user.Password)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) FindUserByEmail(ctx context.Context, email string) (*models.User, error) {
	query := "SELECT id, username, email, password FROM users WHERE email = $1"
	row := r.DB.QueryRowContext(ctx, query, email)
	user := &models.User{}
	if err := row.Scan(&user.ID, &user.Username, &user.Email, &user.Password); err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) FindUserByUsername(ctx context.Context, username string) (*models.User, error) {
	query := "SELECT id, username, email, password FROM users WHERE username = $1"
	row := r.DB.QueryRowContext(ctx, query, username)
	user := &models.User{}
	if err := row.Scan(&user.ID, &user.Username, &user.Email, &user.Password); err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) ListUsers(ctx context.Context, offset, limit int) ([]*models.User, error) {
	query := "SELECT id, username, email, password FROM users ORDER BY id LIMIT $1 OFFSET $2"
	rows, err := r.DB.QueryContext(ctx, query, limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []*models.User
	for rows.Next() {
		user := &models.User{}
		if err := rows.Scan(&user.ID, &user.Username, &user.Email, &user.Password); err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}

func (r *UserRepository) ValidateUser(ctx context.Context, email, password string) (*models.User, error) {
	query := "SELECT id, username, email, password FROM users WHERE email = $1 AND password = $2"
	row := r.DB.QueryRowContext(ctx, query, email, password)
	user := &models.User{}
	if err := row.Scan(&user.ID, &user.Username, &user.Email, &user.Password); err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) UpdateRefreshToken(ctx context.Context, userID int64, refreshToken string) error {
	query := "UPDATE users SET refresh_token = $1 WHERE id = $2"
	_, err := r.DB.ExecContext(ctx, query, refreshToken, userID)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) FindUserByRefreshToken(ctx context.Context, refreshToken string) (*models.User, error) {
	query := "SELECT id, username, email, password FROM users WHERE refresh_token = $1"
	row := r.DB.QueryRowContext(ctx, query, refreshToken)
	user := &models.User{}
	if err := row.Scan(&user.ID, &user.Username, &user.Email, &user.Password); err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		return nil, err
	}
	return user, nil
}
