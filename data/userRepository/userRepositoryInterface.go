package userRepository

import (
	"context"
	"project-1/models"
)

type UserRepositoryInterface interface {
	CreateUser(ctx context.Context, user *models.User) error
	FindUserByEmail(ctx context.Context, email string) (*models.User, error)
	FindUserByUsername(ctx context.Context, username string) (*models.User, error)
	ListUsers(ctx context.Context, offset, limit int) ([]*models.User, error)
	ValidateUser(ctx context.Context, email, password string) (*models.User, error)
	UpdateRefreshToken(ctx context.Context, userID int64, refreshToken string) error
	FindUserByRefreshToken(ctx context.Context, refreshToken string) (*models.User, error)
}
